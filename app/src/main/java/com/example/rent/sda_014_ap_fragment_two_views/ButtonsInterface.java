package com.example.rent.sda_014_ap_fragment_two_views;

/**
 * Created by Pawel on 25.04.2017.
 */

public interface ButtonsInterface {
    void showBasicData();

    void showDetailData();
}