package com.example.rent.sda_014_ap_fragment_two_views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main2Activity extends AppCompatActivity {

    public static final int BASIC = 0;
    public static final int DETAIL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        // to jest taki sposob przekazania ze wartosc idzie jako default jakby nic nie bylo
        // defaultem jest ten BASIC
        final int data = getIntent().getIntExtra("data", BASIC);
        DetailFragment fragment = (DetailFragment) getFragmentManager().findFragmentById(R.id.detailFragment);

        // przekazujemy wczesniej albo jeden albo zero i teraz staramy sie uzaleznic wyswietlanie od tej liczby czyli zero lub jeden
        if (data == BASIC) {
            fragment.showBasicData();
        } else if (data == DETAIL) {
            fragment.showDetailData();
        }
    }
}