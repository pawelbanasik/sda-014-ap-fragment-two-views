package com.example.rent.sda_014_ap_fragment_two_views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity implements ButtonsInterface {


    private DetailFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment = (DetailFragment) getFragmentManager().findFragmentById(R.id.detailFragment);

        // tego troche nie kumam
        if (fragment != null && fragment.isInLayout()) {
            fragment.showBasicData();
        }
    }

    // nazwy innych metod takie same
    @Override
    public void showBasicData() {
        if (fragment != null && fragment.isInLayout()) {
            fragment.showBasicData();
        }
        else {
            Intent intent = new Intent(this, Main2Activity.class);
            intent.putExtra("data", Main2Activity.BASIC);
            startActivity(intent);
        }
    }

    // nazwy innych metod takie same
    @Override
    public void showDetailData() {
        if (fragment != null && fragment.isInLayout()) {
            fragment.showDetailData();
        }
        else {
            Intent intent = new Intent(this, Main2Activity.class);
            intent.putExtra("data", Main2Activity.DETAIL);
            startActivity(intent);
        }
    }
}