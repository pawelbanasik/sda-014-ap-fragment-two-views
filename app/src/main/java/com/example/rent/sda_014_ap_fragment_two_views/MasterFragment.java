package com.example.rent.sda_014_ap_fragment_two_views;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by RENT on 2017-04-24.
 */

public class MasterFragment extends Fragment {

    private Button basicDataButton;
    private Button detailDataButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);
        basicDataButton = (Button) view.findViewById(R.id.basicDataButton);
        detailDataButton = (Button) view.findViewById(R.id.detailDataButton);

        // pobieram aktywnosc w ktorej fragment master jest umieszczony
        final MainActivity mainActivity = (MainActivity) getActivity();
        basicDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showBasicData();
            }
        });
        detailDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showDetailData();
            }
        });

        return view;
    }
}